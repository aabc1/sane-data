# Corrupted image example for SCX-3205w

* Issue: https://gitlab.com/sane-project/backends/-/issues/714

Images 04, 05, and 13 have `libjpeg-turbo` warning `Corrupt JPEG data: premature end of data segment`.
Scan log with some additional debugging:
```
$ scanimage -d 'xerox_mfp:tcp 10.4.129.85 9400' -o full.jpg
[23:49:34.548730] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 82072, slack: -407144
remove /tmp/stmp_enc-00.jpg for new file
append to image 65536 bytes
append to image 16536 bytes
decompress_tempfile /tmp/stmp_enc-00.jpg
read jpeg file /tmp/stmp_enc-00.jpg
[23:49:35.700804] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 88040, slack: -401176
append to image 65536 bytes
append to image 22504 bytes
decompress_tempfile /tmp/stmp_enc-01.jpg
read jpeg file /tmp/stmp_enc-01.jpg
[23:49:36.788794] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 119316, slack: -369900
append to image 65536 bytes
append to image 53780 bytes
decompress_tempfile /tmp/stmp_enc-02.jpg
read jpeg file /tmp/stmp_enc-02.jpg
[23:49:37.940782] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 112552, slack: -376664
append to image 65536 bytes
append to image 47016 bytes
decompress_tempfile /tmp/stmp_enc-03.jpg
read jpeg file /tmp/stmp_enc-03.jpg
[23:49:39.092782] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 116676, slack: -372540
append to image 65536 bytes
append to image 51140 bytes
decompress_tempfile /tmp/stmp_enc-04.jpg
read jpeg file /tmp/stmp_enc-04.jpg
Corrupt JPEG data: premature end of data segment
[23:49:40.244783] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 118516, slack: -370700
append to image 65536 bytes
append to image 52980 bytes
decompress_tempfile /tmp/stmp_enc-05.jpg
read jpeg file /tmp/stmp_enc-05.jpg
Corrupt JPEG data: premature end of data segment
[23:49:41.396779] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 123324, slack: -365892
append to image 65536 bytes
append to image 57788 bytes
decompress_tempfile /tmp/stmp_enc-06.jpg
read jpeg file /tmp/stmp_enc-06.jpg
[23:49:42.484789] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 131376, slack: -357840
append to image 65536 bytes
append to image 65536 bytes
append to image 304 bytes
decompress_tempfile /tmp/stmp_enc-07.jpg
read jpeg file /tmp/stmp_enc-07.jpg
[23:49:43.636793] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 125908, slack: -363308
append to image 65536 bytes
append to image 60372 bytes
decompress_tempfile /tmp/stmp_enc-08.jpg
read jpeg file /tmp/stmp_enc-08.jpg
[23:49:44.788792] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 119636, slack: -369580
append to image 65536 bytes
append to image 54100 bytes
decompress_tempfile /tmp/stmp_enc-09.jpg
read jpeg file /tmp/stmp_enc-09.jpg
[23:49:45.876787] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 91548, slack: -397668
append to image 65536 bytes
append to image 26012 bytes
decompress_tempfile /tmp/stmp_enc-10.jpg
read jpeg file /tmp/stmp_enc-10.jpg
[23:49:46.964781] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 66048, slack: -423168
append to image 65536 bytes
append to image 512 bytes
decompress_tempfile /tmp/stmp_enc-11.jpg
read jpeg file /tmp/stmp_enc-11.jpg
[23:49:48.052780] [xerox_mfp] acquiring, size per band v: 128, h: 1274, block: 57940, slack: -431276
append to image 57940 bytes
decompress_tempfile /tmp/stmp_enc-12.jpg
read jpeg file /tmp/stmp_enc-12.jpg
[23:49:49.140771] [xerox_mfp] acquiring, size per band v: 64, h: 1274, last block: 18692, slack: -225916
append to image 18692 bytes
decompress_tempfile /tmp/stmp_enc-13.jpg
read jpeg file /tmp/stmp_enc-13.jpg
Corrupt JPEG data: premature end of data segment
```
